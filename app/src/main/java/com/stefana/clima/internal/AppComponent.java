package com.stefana.clima.internal;

import com.stefana.clima.views.activities.MainActivity;
import com.stefana.clima.views.fragments.FiveDaysWeatherFragment;
import com.stefana.clima.views.fragments.TodayWeatherFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton @Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(ClimaApplication climaApplication);
    void inject(MainActivity mainActivity);

    void inject(TodayWeatherFragment todayWeatherFragment);
    void inject(FiveDaysWeatherFragment fiveDaysWeatherFragment);

}
