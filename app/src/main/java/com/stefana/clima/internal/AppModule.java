package com.stefana.clima.internal;

import android.content.Context;
import android.content.res.Resources;

import com.stefana.clima.ApiService;
import com.stefana.clima.repositories.WeatherRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class AppModule {

    private final ClimaApplication climaApplication;
    private ApiService apiService;

    public AppModule(ClimaApplication climaApplication) {
        this.climaApplication = climaApplication;
    }

    @Provides
    @Singleton
    Context provideAppContext(){
        return climaApplication;
    }

    @Provides
    Resources getResources(Context context){
        return context.getResources();
    }

    @Singleton
    @Provides
    ApiService getApiService(Context context){
        if(apiService==null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(WeatherRepository.API)
                    .addConverterFactory(JacksonConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiService.class);
        }

        return apiService;
    }
}
