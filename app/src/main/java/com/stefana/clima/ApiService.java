package com.stefana.clima;

import com.stefana.clima.dtos.CurrentWeather;
import com.stefana.clima.dtos.WeatherForecast;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("weather")
    Call<CurrentWeather> getCurrentWeather(@Query("q") String cityQuery,
                                           @Query("units") String units,
                                           @Query("appid") String appId);

    @GET("forecast")
    Call<WeatherForecast> getWeatherForecast(@Query("q") String cityQuery,
                                             @Query("units") String units,
                                             @Query("appid") String appId);
    @GET("weather")
    Call<CurrentWeather> getCurrentWeather(@Query("lat") double latitude,
                                           @Query("lon") double longitude,
                                           @Query("units") String units,
                                           @Query("appid") String appId);

    @GET("forecast")
    Call<WeatherForecast> getWeatherForecast(@Query("lat") double latitude,
                                           @Query("lon") double longitude,
                                           @Query("units") String units,
                                           @Query("appid") String appId);
}
