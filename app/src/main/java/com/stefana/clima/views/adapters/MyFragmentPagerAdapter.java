package com.stefana.clima.views.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.stefana.clima.R;
import com.stefana.clima.views.fragments.FiveDaysWeatherFragment;
import com.stefana.clima.views.fragments.TodayWeatherFragment;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private TodayWeatherFragment todayFramgnet;
    private FiveDaysWeatherFragment fiveDayFragment;

    public MyFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return todayFramgnet = new TodayWeatherFragment();
        } else if (position == 1) {
            return fiveDayFragment = new FiveDaysWeatherFragment();
        }

        return null;
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return mContext.getString(R.string.today);
            case 1:
                return mContext.getString(R.string.five_days);

            default:
                return null;
        }
    }

    public void setCity(String city) {
        todayFramgnet.setCity(city);
        fiveDayFragment.setCity(city);
    }

    public void setCoordinates(double latitude, double longitude) {
        todayFramgnet.setCoordinates(latitude, longitude);
        fiveDayFragment.setCoordinates(latitude, longitude);


    }

    public void destroy() {
        mContext = null;
        todayFramgnet = null;
        fiveDayFragment = null;
    }
}
