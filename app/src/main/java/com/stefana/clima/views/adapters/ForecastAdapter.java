package com.stefana.clima.views.adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.stefana.clima.R;
import com.stefana.clima.dtos.ImageMapper;
import com.stefana.clima.dtos.WeatherStateForecast;
import com.stefana.clima.views.adapters.view_holders.ForecastViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastViewHolder> {


    private List<WeatherStateForecast> items = new ArrayList<>();

    public void setItems(List<WeatherStateForecast> items) {
        this.items = items;
    }


    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ForecastViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.forecast_view_holder, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder forecastViewHolder, int i) {
        String degreeCelsius = (char) 0x00B0 + " C";

        WeatherStateForecast forecast = items.get(i + 1);
        Glide.with(forecastViewHolder.itemView.getContext())
                .load(ImageMapper.getWeatherResourceById(forecast.getWeather()[0].getId())).fitCenter()
                .into(forecastViewHolder.image);

        Resources res = forecastViewHolder.itemView.getContext().getResources();
        forecastViewHolder.time.setText(res.getString(R.string.time, forecast.getDt_txt()));
        forecastViewHolder.temp.setText(res.getString(R.string.temp, forecast.getMain().getTemp(), degreeCelsius));
        forecastViewHolder.minMaxTemp.setText(res.getString(R.string.min_max_temp, forecast.getMain().getTemp_min(), degreeCelsius,
                forecast.getMain().getTemp_max(), degreeCelsius));
        forecastViewHolder.humidity.setText(res.getString(R.string.huumidity, forecast.getMain().getHumidity(), " %"));
        forecastViewHolder.windSpeed.setText(res.getString(R.string.wind_speed, forecast.getWind().getSpeed()));
        forecastViewHolder.pressure.setText(res.getString(R.string.pressure, forecast.getMain().getPressure()));
        forecastViewHolder.description.setText(res.getString(R.string.description, forecast.getWeather()[0].getDescription()));
    }

    @Override
    public int getItemCount() {
        return items.size() - 1;
    }
}
