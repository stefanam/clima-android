package com.stefana.clima.views.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import com.stefana.clima.R;
import com.stefana.clima.internal.ClimaApplication;
import com.stefana.clima.presenters.MainPresenter;
import com.stefana.clima.views.adapters.MyFragmentPagerAdapter;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainPresenter.MainView {

    private static final int REQUEST_CODE = 101;
    private MyFragmentPagerAdapter adapter;
    private TextView cityDisplay;

    @Inject
    MainPresenter mainPresenter;

    ViewPager viewPager;

    private LocationManager locationManager;
    private LocationListener locationListener;
    private TabLayout tabLayout;
    private ProgressBar progressBar;
    private CursorAdapter mAdapter;
    private static String[] SUGGESTIONS = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_main);

        ((ClimaApplication) getApplication()).getAppComponent().inject(this);

        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tab_layout);
        cityDisplay = findViewById(R.id.city_display);
        adapter = new MyFragmentPagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        progressBar = findViewById(R.id.progressBar1);
        final String[] from = new String[] {"cityName"};
        final int[] to = new int[] {android.R.id.text1};
        SUGGESTIONS = getResources().getStringArray(R.array.cities);
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.city_item, null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                cityDisplay.setText(query);
                adapter.setCity(query);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                populateAdapter(newText);
                return false;
            }
        });

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String cityName = cursor.getString(cursor.getColumnIndex("cityName"));
                searchView.setQuery(cityName, true);
                return true;
            }
        });

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {

            return true;
        } else if (id == R.id.action_get_current_location) {
            loadLocation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.onResume(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadLocation();
            }
        }, 1000);

    }


    @Override
    protected void onPause() {
        super.onPause();
        mainPresenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.destroy();
        adapter = null;
        mainPresenter.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        loadLocation();
                    }
                }
            }
        }
    }

    public void showProgress(boolean state){
        progressBar.setVisibility(state ? View.VISIBLE : View.GONE);
    }

    public void loadLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            String[] permissions = new String[2];
            permissions[0] = Manifest.permission.ACCESS_FINE_LOCATION;
            permissions[1] = Manifest.permission.ACCESS_COARSE_LOCATION;

            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE);


            return;
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        com.google.android.gms.location.LocationListener mLocationListener = new com.google.android.gms.location.LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                //listener.onLocation(location);
                presentLocation(location);
            }
        };
        if (location != null) {
            // listener.onLocation(location);
            presentLocation(location);
        } else {
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    // listener.onLocation(location);
                    locationManager.removeUpdates(locationListener);
                    presentLocation(location);
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {

                }

                @Override
                public void onProviderEnabled(String s) {

                }

                @Override
                public void onProviderDisabled(String s) {

                }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    }


    public String decodeLocation(double lat, double lon) {

        String city = "";

        Geocoder geoCoder = new Geocoder(MainActivity.this, Locale.getDefault()); //it is Geocoder
        try {
            List<Address> address = geoCoder.getFromLocation(lat, lon, 1);
            city = address.get(0).getLocality();


        } catch (IOException e) {

        } catch (NullPointerException e) {
        }
        return city;
    }

    public void presentLocation(Location location) {

        String city = decodeLocation(location.getLatitude(), location.getLongitude());

        cityDisplay.setText(city);

        adapter.setCoordinates(location.getLatitude(), location.getLongitude());
    }

    private void populateAdapter(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
        for (int i=0; i<SUGGESTIONS.length; i++) {
            if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                c.addRow(new Object[] {i, SUGGESTIONS[i]});
        }
        mAdapter.changeCursor(c);
    }
}
