package com.stefana.clima.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.stefana.clima.R;
import com.stefana.clima.views.adapters.items.ListItem;
import com.stefana.clima.views.adapters.view_holders.TodayViewHolder;

import java.util.ArrayList;
import java.util.List;

public class TodayAdapter extends RecyclerView.Adapter<TodayViewHolder> {

    private List<ListItem> items = new ArrayList<>();

    public void setItems(List<ListItem> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public TodayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TodayViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.today_view_holder, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull TodayViewHolder todayViewHolder, int i) {
        ListItem item = items.get(i);
        todayViewHolder.text.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
