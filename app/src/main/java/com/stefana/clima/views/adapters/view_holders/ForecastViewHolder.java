package com.stefana.clima.views.adapters.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stefana.clima.R;

public class ForecastViewHolder extends RecyclerView.ViewHolder {

    public ImageView image;
    public TextView time;
    public TextView temp;
    public TextView minMaxTemp;
    public TextView humidity;
    public TextView windSpeed;
    public TextView pressure;
    public TextView description;

    public ForecastViewHolder(@NonNull View itemView) {
        super(itemView);

        image = itemView.findViewById(R.id.image);
        time = itemView.findViewById(R.id.time);
        temp = itemView.findViewById(R.id.temp);
        minMaxTemp = itemView.findViewById(R.id.min_max_temp);
        humidity = itemView.findViewById(R.id.humidity);
        windSpeed = itemView.findViewById(R.id.wind_speed);
        pressure = itemView.findViewById(R.id.pressure);
        description = itemView.findViewById(R.id.description);

    }
}
