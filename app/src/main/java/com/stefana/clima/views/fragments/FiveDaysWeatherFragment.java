package com.stefana.clima.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.stefana.clima.R;
import com.stefana.clima.dtos.WeatherStateForecast;
import com.stefana.clima.internal.ClimaApplication;
import com.stefana.clima.presenters.FiveDaysWeatherFragmentPresenter;
import com.stefana.clima.views.activities.MainActivity;
import com.stefana.clima.views.adapters.ForecastAdapter;

import java.util.List;

import javax.inject.Inject;

public class FiveDaysWeatherFragment extends BaseFragment implements FiveDaysWeatherFragmentPresenter.View {

    @Inject
    FiveDaysWeatherFragmentPresenter presenter;

    private ForecastAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_five_days_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ClimaApplication) getActivity().getApplication()).getAppComponent().inject(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView = view.findViewById(R.id.content);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ForecastAdapter();
        recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);


    }



    @Override
    public void presentData(List<WeatherStateForecast> items) {
        ((MainActivity)getActivity()).showProgress(false);
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String localizedMessage) {
        Snackbar.make(getView(), R.string.network_error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void noCityError() {
        Snackbar.make(getView(), R.string.no_city_found, Snackbar.LENGTH_SHORT).show();

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume(this);
        //presenter.load("Belgrade");

    }

    @Override
    public void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


    @Override
    public void setCity(String city) {
        ((MainActivity)getActivity()).showProgress(true);
        presenter.load(city);
    }

    @Override
    public void setCoordinates(double latitude, double longitude) {
        ((MainActivity)getActivity()).showProgress(true);
        presenter.load(latitude, longitude);
    }
}
