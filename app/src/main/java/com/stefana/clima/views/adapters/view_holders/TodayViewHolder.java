package com.stefana.clima.views.adapters.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.stefana.clima.R;

public class TodayViewHolder extends RecyclerView.ViewHolder {

    public TextView text;

    public TodayViewHolder(@NonNull View itemView) {
        super(itemView);
        text = itemView.findViewById(R.id.text);
    }
}
