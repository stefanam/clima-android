package com.stefana.clima.views.adapters.items;

public class ListItem {

    private String description;

    public ListItem(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
