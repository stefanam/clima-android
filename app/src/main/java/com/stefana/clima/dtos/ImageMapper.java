package com.stefana.clima.dtos;

import com.stefana.clima.R;

public class ImageMapper {

    public static int getWeatherResourceById(long id){

        if (id < 300){
            return R.drawable.tstorm1;
        } if (id < 500){
            return R.drawable.light_rain;
        } if (id < 600){
            return R.drawable.shower3;
        } if (id < 700){
            return R.drawable.snow4;

        } if (id < 771){
            return R.drawable.fog;

        } if (id < 799){
            return R.drawable.tstorm3;

        } if (id == 800){
            return R.drawable.sunny;

        } if (id < 804){
            return R.drawable.cloudy2;

        } if(id == 804){
          return R.drawable.overcast;
        } if (id > 900 && id < 903 || id > 905 && id < 1000){
            return R.drawable.tstorm3;

        } if (id == 903){
            return R.drawable.snow5;

        } if (id == 904){
            return R.drawable.sunny;

        } else {
            return R.drawable.dunno;
        }
    }
}
