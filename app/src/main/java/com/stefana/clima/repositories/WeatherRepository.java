package com.stefana.clima.repositories;

import com.stefana.clima.ApiService;
import com.stefana.clima.dtos.CurrentWeather;
import com.stefana.clima.dtos.WeatherForecast;
import java.io.IOException;
import javax.inject.Inject;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WeatherRepository {

    private static final String APP_ID = "facb5c13d77a1077b2843c59ad705dbf";
    public static final String API = "https://api.openweathermap.org/data/2.5/";

    private String units = "metric";//or imperial
    private ApiService service;

    @Inject
    public WeatherRepository(ApiService service) {
        this.service = service;
    }

    public Observable<CurrentWeather> getCurrentWeather(final String query) {
        return Observable.create(new Observable.OnSubscribe<CurrentWeather>() {
            @Override
            public void call(Subscriber<? super CurrentWeather> subscriber) {
                try {
                    subscriber.onNext(service.getCurrentWeather(query, units, APP_ID).execute().body());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<WeatherForecast> getWeatherForecast(final String query) {
        return Observable.create(new Observable.OnSubscribe<WeatherForecast>() {
            @Override
            public void call(Subscriber<? super WeatherForecast> subscriber) {
                try {
                    subscriber.onNext(service.getWeatherForecast(query, units, APP_ID).execute().body());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<CurrentWeather> getCurrentWeather(final double latitude, final double longitude) {
        return Observable.create(new Observable.OnSubscribe<CurrentWeather>() {
            @Override
            public void call(Subscriber<? super CurrentWeather> subscriber) {
                try {
                    CurrentWeather local = service.getCurrentWeather(latitude, longitude, units, APP_ID).execute().body();
                    subscriber.onNext(local);
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public Observable<WeatherForecast> getWeatherForecast(final double latitude, final double longitude) {
        return Observable.create(new Observable.OnSubscribe<WeatherForecast>() {
            @Override
            public void call(Subscriber<? super WeatherForecast> subscriber) {
                try {
                    subscriber.onNext(service.getWeatherForecast(latitude, longitude, units, APP_ID).execute().body());
                } catch (IOException e) {
                    subscriber.onError(e);
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }



}
