package com.stefana.clima.presenters;

interface Presenter<T> {

    void onResume(T view);

    void onPause();

    void onDestroy();

}
