package com.stefana.clima.presenters;

import com.stefana.clima.dtos.WeatherForecast;
import com.stefana.clima.dtos.WeatherStateForecast;
import com.stefana.clima.repositories.WeatherRepository;

import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class FiveDaysWeatherFragmentPresenter implements Presenter<FiveDaysWeatherFragmentPresenter.View>{

    private WeatherRepository repository;
    private View view;

    @Inject
    public FiveDaysWeatherFragmentPresenter(WeatherRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onResume(View view) {
        this.view = view;
    }

    @Override
    public void onPause() {
        this.view = null;
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    public void load(String city) {

        repository.getWeatherForecast(city).subscribe(new Action1<WeatherForecast>() {
                @Override
                public void call(WeatherForecast fiveDaysWeather) {
                    if (view != null) {

                        if(fiveDaysWeather == null){
                            view.noCityError();
                            return;
                        }
                        view.presentData(fiveDaysWeather.getList());

                    }
                }
            }, new Action1<Throwable>() {
                @Override
                public void call(Throwable throwable) {
                    if (view != null) {
                        throwable.printStackTrace();
                        view.showError(throwable.getLocalizedMessage());
                    }
                }
            });
    }

    public void load(double latitude, double longitude) {

        repository.getWeatherForecast(latitude, longitude).subscribe(new Action1<WeatherForecast>() {
            @Override
            public void call(WeatherForecast fiveDaysWeather) {
                if (view != null) {

                    if(fiveDaysWeather == null){
                        view.noCityError();
                        return;
                    }
                    view.presentData(fiveDaysWeather.getList());

                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                if (view != null) {
                    throwable.printStackTrace();
                    view.showError(throwable.getLocalizedMessage());
                }
            }
        });
    }

    public interface View {
        void presentData(List<WeatherStateForecast> items);

        void showError(String localizedMessage);

        void noCityError();
    }
}
