package com.stefana.clima.presenters;

import android.content.Context;
import android.content.res.Resources;
import com.stefana.clima.R;
import com.stefana.clima.dtos.CurrentWeather;
import com.stefana.clima.dtos.ImageMapper;
import com.stefana.clima.repositories.WeatherRepository;
import com.stefana.clima.views.adapters.items.ListItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.functions.Action1;

public class TodayWeatherFragmentPresenter implements Presenter<TodayWeatherFragmentPresenter.View> {

    private View view;
    private WeatherRepository repository;


    @Inject
    public TodayWeatherFragmentPresenter(WeatherRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onResume(View view) {
        this.view = view;
    }

    @Override
    public void onPause() {
        this.view = null;
    }

    @Override
    public void onDestroy() {
        this.view = null;

    }



    public void load(String query) {

        repository.getCurrentWeather(query).subscribe(new Action1<CurrentWeather>() {
            @Override
            public void call(CurrentWeather currentWeather) {
                if (view != null) {

                    mapData(currentWeather);

                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                if (view != null) {
                    throwable.printStackTrace();
                    view.showError(throwable.getLocalizedMessage());
                }
            }
        });
    }

    private void mapData(CurrentWeather currentWeather) {

        if (currentWeather == null) {
            view.noCityError();
            return;
        }
        int weatherIconType = currentWeather.getWeather().get(0).getId();

        view.setImageIcon(ImageMapper.getWeatherResourceById(weatherIconType));

        String degreeCelsius = (char) 0x00B0 + " C";
        Resources res = view.getContext().getResources();
        List<ListItem> items = new ArrayList<>();

        items.add(new ListItem(res.getString(R.string.temp, currentWeather.getMain().getTemp(), degreeCelsius)));
        items.add(new ListItem(res.getString(R.string.min_temp, currentWeather.getMain().getTemp_min(), degreeCelsius)));
        items.add(new ListItem(res.getString(R.string.max_temp, currentWeather.getMain().getTemp_max(), degreeCelsius)));

        items.add(new ListItem(res.getString(R.string.huumidity, currentWeather.getMain().getHumidity(), " %")));

        items.add(new ListItem(res.getString(R.string.wind_speed, currentWeather.getWind().getSpeed())));
        items.add(new ListItem(res.getString(R.string.pressure, currentWeather.getMain().getPressure())));

        items.add(new ListItem(res.getString(R.string.description, currentWeather.getWeather().get(0).getDescription())));

        view.presentData(items);
    }

    public void load(double latitude, double longitude) {

        repository.getCurrentWeather(latitude, longitude).subscribe(new Action1<CurrentWeather>() {
            @Override
            public void call(CurrentWeather currentWeather) {
                if (view != null) {

                    mapData(currentWeather);


                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                if (view != null) {
                    throwable.printStackTrace();
                    view.showError(throwable.getLocalizedMessage());
                }
            }
        });
    }

    public interface View {

        void presentData(List<ListItem> items);

        void showError(String localizedMessage);

        void setImageIcon(int weatherResourceById);

        void noCityError();

        Context getContext();
    }
}
