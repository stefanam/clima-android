package com.stefana.clima.presenters;

import com.stefana.clima.repositories.WeatherRepository;
import javax.inject.Inject;

public class MainPresenter implements Presenter<MainPresenter.MainView> {

    private MainView view;

    private WeatherRepository repository;

    @Inject
    public MainPresenter(WeatherRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onResume(MainView view) {
        this.view = view;
    }

    @Override
    public void onPause() {
        this.view = null;
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }

    public void setCity(String query) {
       // repository.setCity(query);
    }

    public interface MainView {

    }
}
